const express = require("express");
require('./config/dotenv')();
require("./config/sequelize");

const app = express();
const PORT = process.env.PORT;
const cors = require("cors");
const routes = require("./routes/routes");


app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());
app.use(routes);

app.get('/', (req, res) => {
  res.send('Hello World!')
});

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});








