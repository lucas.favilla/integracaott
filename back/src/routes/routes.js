const {Router} = require("express")

const userControler = require('../controller/userController');
const authController = require('../controller/authControler')

const router = Router();
//user
router.post('/user', userControler.createUser);
router.get('/users', userControler.getAllUsers);

//login
router.post('/login', authController.tryLogin),
router.get('/userInfo', authController.getDetails),
module.exports = router;
