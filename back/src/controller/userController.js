const User =require("../models/User.js");
const Auth = require("../config/Auth");

async function createUser(req, res) {
    try {
        const {
            username,
            password
        } = req.body
        const hashSalt = Auth.generatePassword(password);
        const user = User.create({username: username, hash:hashSalt.hash, salt:hashSalt.salt})
        return res.status(200).json("usuario foi")
    } catch (error) {
        return res.status(500).json('Error ' + error );
    }
}

async function getAllUsers(req, res){
    try {
        const users = await User.findAll()
        return res.status(200).json({users});
    } catch (error) {
        return res.status(500).json('Error ' + error );
    }
}

    module.exports = {createUser, getAllUsers};