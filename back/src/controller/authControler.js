const Auth = require('../config/Auth');
const User = require("../models/User");

async function tryLogin(req, res){
    try {
        const user = await User.findOne({where: {username: req.body.username}})
        if(!user){
            return res.status(404).json({message: "treinador Não encontrado"});
        }
        const {password} = req.body
        if(Auth.checkPassword(password, user.hash , user.salt)){
            const token = Auth.generateJWT(user);
            res.status(200).json({token});
        } 
        else {
            return res.status(401).json('Senha incorreta')
        }
    }
    catch (err) {
        return res.status(500).json({ err })
    }
}

async function getDetails(req, res) {
    try {
        const token = Auth.getToken(req);
        const payload = Auth.decodeJwt(token);
        const user = await User.findByPk(payload.sub);
        if (!user)
            return res.status(404).json({ message: "Treinador não encontrado" });
        return res.status(200).json({ user: user });
    } catch (err) {
        return res.status(500).json({ err })
    }
}

module.exports = {tryLogin, getDetails}
